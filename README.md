# GSoC Inteview Practice

This is a repo meant for practicing my GSoC inteview for project `libvirt`.

I will be documenting my experience in each one of the year questions and
a note on how to improve.

## 2013

The problem was relatively easy, but the question is a bit vague left for the
implementer's understanding of the question. There is no information about what
kind of input will be taken, could be string, could be binary. To leave room
for this I have used `read()` function instead of scanf to have the ability to
read not only from stdin but any file. But since the
hash function mentioned requires ascii values of the strings to be added my
implementation was to typecast the string.

Some more assumptions made were that, we ignore newline characters from stdin.
The return values, `true` and `false` seem to be vague and exact reason for
returning `false` is not specifying, I used it when the writes to the file pointer
is unsuccessfull.

## 2014

This question was useful for preperation because it involved both a hashmap and
a doubly linked-list. I didn't really do what the question asked (to use
timestamps), I instead used LRUCache model algorithm, which brings an
element to the front everytime it is visited.

If there is a one hour constraint, I would just maintain a min heap instead.

## 2016

## 2017

## 2018

## 2019

## 2020

## 2021

## 2022

## 2023
