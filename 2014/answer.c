/*
 * Peer manager
 *
 * The peer manager keeps track of neighbor nodes in a peer-to-peer
 * network.  This module might be used as part of a BitTorrent, BitCoin,
 * or Tor client.
 *
 * Every time there is activity from a peer, the peer manager is told about it.
 * The peer manager remembers the n most recently active unique peers.
 *
 * When we wish to communicate, the peer manager can select a random
 * peer.
 *
 * Example usage:
 *
 *   PeerManager mgr;
 *   peermgr_init(&mgr, 8);
 *
 *   peermgr_saw_peer(&mgr, 1234, time(NULL));
 *   peermgr_saw_peer(&mgr, 5432, time(NULL));
 *
 *   PeerId random_peer;
 *   if (peermgr_pick_random_peer(&mgr, &random_peer)) {
 *       printf("chosen peer = %" PRIu64 "\n", random_peer);
 *   }
 *
 *   peermgr_cleanup(&mgr);
 */

#include <glib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* TODO You may include additional C standard library headers and define
 * additional structs.  You cannot change the function prototypes, the API is
 * fixed.
 */

/** A unique identifier for a peer */
typedef uint64_t PeerId;

typedef struct
{

} PeedId;

typedef struct
{
    GList *list;
    GHashTable *table;
    guint maxpeers;
} PeerManager;

/**
 * Initialize @mgr so that it can track up to @maxpeers peers.
 */
void
peermgr_init(PeerManager *mgr, unsigned int maxpeers)
{
    *mgr = (PeerManager){
        .list = NULL,
        .table = g_hash_table_new(g_direct_hash, g_direct_equal),
        .maxpeers = maxpeers,
    };
}

/**
 * Free any resources allocated by @mgr.
 */
void
peermgr_cleanup(PeerManager *mgr)
{
    g_list_free(mgr->list);
    g_hash_table_destroy(mgr->table);
}

/**
 * Update peer information with new activity from @peer at time @timestamp.
 * The peer manager retains a fixed number of unique peers with the most recent
 * timestamps.  The maximum number of peers to remember was set in
 * peermgr_init().
 *
 * If the maximum number of peers to remember has been reached, it may be
 * necessary to forget about the peer with the oldest timestamp so that there
 * is space for the newer peer.
 */
void
peermgr_saw_peer(PeerManager *mgr, PeerId peer, time_t timestamp)
{
    if (g_hash_table_size(mgr->table) == mgr->maxpeers)
    {
        GList *node = g_list_last(mgr->list);
        g_hash_table_remove(mgr->table, node->data);
        mgr->list = g_list_delete_link(mgr->list, node);
    }

    if (g_hash_table_contains(mgr->table, GINT_TO_POINTER(peer)))
    {
        /* if capacity is full remove the last one*/

        /* remove element if already exists */
        mgr->list = g_list_remove_all(mgr->list, GINT_TO_POINTER(peer));
    }

    /* append new element at start */
    mgr->list = g_list_prepend(mgr->list, GINT_TO_POINTER(peer));
    g_hash_table_replace(mgr->table, GINT_TO_POINTER(peer),
                         g_list_first(mgr->list));
}

/**
 * Select a peer at random and store its identifier in @peer.
 *
 * Returns: true on success
 */
bool
peermgr_pick_random_peer(PeerManager *mgr, PeerId *peer)
{
    guint n = random() % g_hash_table_size(mgr->table);
    GList *node = g_list_nth(mgr->list, n);
    *peer = GPOINTER_TO_INT(node->data);
    mgr->list = g_list_delete_link(mgr->list, node);
    /* bring the acceced element to first */
    mgr->list = g_list_prepend(mgr->list, node->data);
    if (*peer != 0)
    {
        return true;
    }
    return false;
}

void
print_table(PeerManager mgr)
{
    gpointer key, value;
    GHashTableIter iter;
    g_hash_table_iter_init(&iter, mgr.table);

    while (g_hash_table_iter_next(&iter, &key, &value))
    {
        printf("%d %d\n", GPOINTER_TO_INT(key),
               GPOINTER_TO_INT(((GList *)value)->data));
    }
    printf("\n\n");
}

void
print_list(PeerManager mgr)
{
    for (GList *node = g_list_first(mgr.list); node; node = node->next)
        printf("%d, ", GPOINTER_TO_INT(node->data));
    printf("\n");
}

int
main()
{
    srandom(time(NULL));
    PeerManager mgr;

    peermgr_init(&mgr, 8);

    peermgr_saw_peer(&mgr, 1234, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 5432, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 6422, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 105472, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 1432, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 3432, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 5432, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 105472, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 2472, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 7472, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_saw_peer(&mgr, 9472, time(NULL));
    print_list(mgr);
    print_table(mgr);

    peermgr_cleanup(&mgr);
}
