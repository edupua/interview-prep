/*
 * Checksummed file writer
 *
 * This module collects data buffers until they should be written out to a
 * file, prefixed by a checksum.
 *
 * Typical usage is:
 *
 *   ChecksumWriter writer;
 *   cwriter_init(&writer);
 *   while (...) {
 *       buf = malloc(len);
 *       ...read len bytes into buf...
 *       cwriter_add_buf(&writer, buf, len);
 *   }
 *   cwriter_finish(&writer, stdout);
 *
 * The ChecksumWriter holds onto the buffers and finally emits the checksum,
 * followed by the buffers themselves.
 *
 * The checksum algorithm is simple: unsigned addition modulo 256 of each byte
 * in the buffer.
 *
 * The output written by cwriter_finish() is laid out like this:
 * [checksum byte][buf1][buf2]...[bufn]
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFER_CAPACITY 100000000
#define MODULO_DIVISOR 256
#define MAX_LEN 1000
typedef long long unsigned int hash_type;

typedef struct
{
    hash_type checksum;
    char *buffer;
    size_t buffer_size;
} ChecksumWriter;

/**
 * cwriter_init:
 * @writer: The writer instance.
 *
 * Initialize a ChecksumWriter so it is ready to be used in cwriter_add_buf()
 * and cwriter_finish() calls.
 */
void
cwriter_init(ChecksumWriter *writer)
{
    *writer = (ChecksumWriter){
        .buffer = (char *)calloc(BUFFER_CAPACITY + 1, sizeof(char)),
        .buffer_size = 0,
        .checksum = 0};
}

/**
 * hash_function:
 * @buffer: take input char* and typecast it into integer and take modulo 256
 * @length: takes the length of the current buffer.
 *
 * This function returns the value after calculating hash_function for given
 * input.
 */
hash_type
hash_function(const char *buffer, size_t length)
{
    hash_type checksum = 0;
    for (size_t i = 0; i < length; i++)
    {
        checksum += (hash_type)buffer[i] % MODULO_DIVISOR;
    }
    return checksum;
}

/**
 * cwriter_add_buf:
 * @writer: The writer instance.
 * @buf: The buffer to add.
 * @len: The length of the buffer, in bytes.
 *
 * Append a buffer to the ChecksumWriter.
 *
 * This function does not copy buf, instead the caller passes ownership of @buf
 * to this function.
 */
void
cwriter_add_buf(ChecksumWriter *writer, void *buf, size_t len)
{
    char *input_buffer = (char *)buf; // typecast to get per byte
    // strcat(writer->buffer, input_buffer);
    snprintf(&writer->buffer[writer->buffer_size],
             BUFFER_CAPACITY - writer->buffer_size, "%s", input_buffer);
    writer->buffer_size += len;
    writer->checksum += hash_function(input_buffer, len);
}

/**
 * cwriter_cleanup:
 * @writer: The writer instance.
 * @fp: The output file.
 *
 * Write the checksum followed by the buffers themselves to @fp.
 *
 * This function frees resources.  To use the ChecksumWriter again, call
 * cwriter_init() first.
 *
 * Returns: true on success, false if writing to file failed
 */
bool
cwriter_finish(ChecksumWriter *writer, FILE *fp)
{
    /* print the checksum */
    char *checksum_str = (char *)calloc(17, sizeof(char));
    size_t checksum_length = snprintf(checksum_str, 16, "%lld", writer->checksum);
    size_t checksum_output_length =
        fwrite(checksum_str, sizeof(char), checksum_length, fp);

    if (checksum_output_length != checksum_length)
    {
        perror("Error: ");
        return false;
    }

    /* print the buffer */
    size_t buffer_output_length =
        fwrite(writer->buffer, sizeof(char), writer->buffer_size, fp);

    if (buffer_output_length != writer->buffer_size)
    {
        perror("Error: ");
        return false;
    }

    free(checksum_str);
    free(writer->buffer);
    return true;
}

/* Main function to test the input. */

int
main()
{
    ChecksumWriter writer;
    cwriter_init(&writer);
    int i = 0;
    while (i < 10)
    {
        char *buf = (char *)calloc(MAX_LEN, sizeof(char));
        printf("Give the %d'th string: ", i);
        fflush(stdout);
        size_t len = read(STDIN_FILENO, buf, MAX_LEN);
        buf[len - 1] = '\0';
        cwriter_add_buf(&writer, buf, MAX_LEN);
        i++;
        free(buf);
    }
    cwriter_finish(&writer, stdout);
    return 0;
}
