/*
 * Checksummed file writer
 *
 * This module collects data buffers until they should be written out to a
 * file, prefixed by a checksum.
 *
 * Typical usage is:
 *
 *   ChecksumWriter writer;
 *   cwriter_init(&writer);
 *   while (...) {
 *       buf = malloc(len);
 *       ...read len bytes into buf...
 *       cwriter_add_buf(&writer, buf, len);
 *   }
 *   cwriter_finish(&writer, stdout);
 *
 * The ChecksumWriter holds onto the buffers and finally emits the checksum,
 * followed by the buffers themselves.
 *
 * The checksum algorithm is simple: unsigned addition modulo 256 of each byte
 * in the buffer.
 *
 * The output written by cwriter_finish() is laid out like this:
 * [checksum byte][buf1][buf2]...[bufn]
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
  /* TODO */
} ChecksumWriter;

/**
 * cwriter_init:
 * @writer: The writer instance.
 *
 * Initialize a ChecksumWriter so it is ready to be used in cwriter_add_buf()
 * and cwriter_finish() calls.
 */
void cwriter_init(ChecksumWriter *writer) { /* TODO */ }

/**
 * cwriter_add_buf:
 * @writer: The writer instance.
 * @buf: The buffer to add.
 * @len: The length of the buffer, in bytes.
 *
 * Append a buffer to the ChecksumWriter.
 *
 * This function does not copy buf, instead the caller passes ownership of @buf
 * to this function.
 */
void cwriter_add_buf(ChecksumWriter *writer, void *buf, size_t len) {
  /* TODO */
}

/**
 * cwriter_cleanup:
 * @writer: The writer instance.
 * @fp: The output file.
 *
 * Write the checksum followed by the buffers themselves to @fp.
 *
 * This function frees resources.  To use the ChecksumWriter again, call
 * cwriter_init() first.
 *
 * Returns: true on success, false if writing to file failed
 */
bool cwriter_finish(ChecksumWriter *writer, FILE *fp) { /* TODO */ }
