#include <glib.h>

typedef struct {
  GArray *heap;
} MinHeap;

MinHeap *min_heap_new() {
  MinHeap *heap = g_new(MinHeap, 1);
  heap->heap =
      g_array_new(FALSE, FALSE, sizeof(int)); // Assuming integers as elements
  return heap;
}

void min_heap_insert(MinHeap *heap, int value) {
  g_array_append_val(heap->heap, value);
  guint index = heap->heap->len - 1;
  while (index > 0 && g_array_index(heap->heap, int, (index - 1) / 2) > value) {
    g_array_index(heap->heap, int, index) =
        g_array_index(heap->heap, int, (index - 1) / 2);
    index = (index - 1) / 2;
  }
  g_array_index(heap->heap, int, index) = value;
}

int min_heap_extract_min(MinHeap *heap) {
  if (heap->heap->len == 0) {
    g_print("Error: Heap is empty.\n");
    return -1; // Or handle error differently
  }

  int min = g_array_index(heap->heap, int, 0);
  int last = g_array_index(heap->heap, int, heap->heap->len - 1);
  heap->heap = g_array_remove_index(heap->heap, heap->heap->len - 1);

  if (heap->heap->len > 0) {
    guint index = 0;
    while (2 * index + 1 < heap->heap->len) {
      guint child = 2 * index + 1;
      if (child + 1 < heap->heap->len &&
          g_array_index(heap->heap, int, child + 1) <
              g_array_index(heap->heap, int, child)) {
        child++;
      }
      if (last <= g_array_index(heap->heap, int, child)) {
        break;
      }
      g_array_index(heap->heap, int, index) =
          g_array_index(heap->heap, int, child);
      index = child;
    }
    g_array_index(heap->heap, int, index) = last;
  }
  return min;
}

void min_heap_free(MinHeap *heap) {
  g_array_free(heap->heap, TRUE);
  g_free(heap);
}

int main() {
  MinHeap *heap = min_heap_new();

  // Example usage: Insert elements into the heap
  min_heap_insert(heap, 10);
  min_heap_insert(heap, 5);
  min_heap_insert(heap, 20);

  // Example usage: Extract the minimum element from the heap
  int min = min_heap_extract_min(heap);
  g_print("Minimum element: %d\n", min);

  min_heap_free(heap);
  return 0;
}
